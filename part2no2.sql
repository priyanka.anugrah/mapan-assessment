-- 2a
SELECT 
	up.Name,
	ud.Email,
	ud.Phone,
	up.Gender,
	up.SchoolYear,
	up.District,
	us.CurrentDiamond,
	us.SubscriptionEndDate 
FROM user_profile up
left join user_data ud on up.UniqueId = ud.UniqueId 
left join user_status us on up.UniqueId = us.UniqueId 
WHERE DATE(us.SubscriptionEndDate) > CURDATE();
-- ------------------------------------------------
-- 2b
WITH profile AS (
SELECT
		up.Name,
		ud.Email,
		up.UniqueId
FROM user_profile up 
LEFT JOIN user_data ud ON up.UniqueId = ud.UniqueId 
),

detail_transaction AS (
SELECT
	t.UniqueTransactionId,
	t.UniqueUserId,
	p.PackageName,
	p.TotalPrice,
	t.CompletedOrderTransactionDate 
FROM `transaction` t
LEFT JOIN packages p ON t.UniquePackageId = p.PackageId
)

SELECT 
		dt.UniqueTransactionID,
		dt.UniqueUserID,
		p.Email,
		p.Name,
		dt.PackageName,
		dt.TotalPrice,
		dt.CompletedOrderTransactionDate
FROM profile p
LEFT JOIN detail_transaction dt on p.UniqueID = dt.UniqueUserID
-- ----------------------------------------------

-- 2c
WITH detail_transaction AS (
SELECT
	t.UniqueTransactionId,
	t.UniqueUserId,
	p.PackageName,
	p.TotalPrice,
	t.CompletedOrderTransactionDate 
FROM `transaction` t
LEFT JOIN packages p ON t.UniquePackageId = p.PackageId
)

SELECT 
	up.SchoolYear,
	sum(dt.TotalPrice) as TotalRevenue
FROM detail_transaction dt
LEFT JOIN user_profile up ON up.UniqueId = dt.UniqueUserID
GROUP BY up.SchoolYear 
ORDER BY up.SchoolYear ASC 
-- ----------------------------------------------
-- 2d
WITH detail_transaction AS (
SELECT
	t.UniqueTransactionId,
	t.UniqueUserId,
	p.PackageName,
	t.CompletedOrderTransactionDate 
FROM `transaction` t
LEFT JOIN packages p ON t.UniquePackageId = p.PackageId
)

SELECT 
	UniqueUserID,
	Email,
	FirstPurchaseDate,
	FirstPurchasedPackageName
FROM (SELECT 
	RANK () OVER(PARTITION BY ud.Email ORDER BY dt.CompletedOrderTransactionDate ASC)  as DateHistory,
	dt.UniqueUserId,
	ud.Email,
	dt.CompletedOrderTransactionDate AS FirstPurchaseDate,
	dt.PackageName as FirstPurchasedPackageName
FROM detail_transaction dt
LEFT JOIN user_data ud ON dt.UniqueUserID = ud.UniqueId) a 
WHERE DateHistory = 1 AND FirstPurchaseDate IS NOT NULL
